'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const express = require('express');
const router = express.Router();
const path = require('path');

router.get('/', (req, res, next) => {
	res.sendFile(path.join(__dirname, '../public/html/editor/index.html'));
});

module.exports = router;
