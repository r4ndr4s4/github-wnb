'use strict';

angular.module('wnb').controller('ViewerController', function ($scope, $sce, $location, ViewerService) {
	const url = $location.url();
	const publicID = url.slice(1, url.length);

	let vm = this;

	ViewerService.post({ publicID: publicID }, function (response) {
		vm.content = $sce.trustAsHtml(response.content);
		vm.title = response.title;
	});
});
