'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

const db = mongoose.connection;

mongoose.Promise = global.Promise;
mongoose.connect(settings.dbURL);
