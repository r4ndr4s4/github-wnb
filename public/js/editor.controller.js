'use strict';

angular.module('wnb').controller('EditorController', function ($scope, $rootScope, $window, $filter, orderByFilter, ShareService) {
	let vm = this;

	vm.colors = ['red', 'blue', 'green', 'yellow', 'brown', 'grey', 'teal'];

	vm.orderBy = ['state', '-lastMod']; // used in HTML and controller also
	vm.filter = () => (item) => item.state < 3;  // used in HTML and controller also

	// * * *

	vm.selectItem = function (itemID) {
		const items = angular.fromJson($rootScope.localStorage.getItem('wnb.items'));
		const item = items.find(item => item._id === itemID);

		vm.itemID = itemID; // set as selected item
		vm.type = item.type; // set for note/task type
		vm.state = item.state; // set for task checked state
		vm.title = item.title; // set for edit mode
		vm.content = item.content; // set for edit mode

		angular.element('.navbar-collapse').collapse('hide'); // close navbar on mobile
	};

	$rootScope.items.forEach((localItem, index, items) => {
		if (!localItem.content && localItem.lastSyncedVersion === null) {
			items[index].state = 4; // hide empty, new items
		}
	});

	const filteredItems = $filter('filter')($rootScope.items, vm.filter());

	if (filteredItems.length > 0) {
		const orderedItems = orderByFilter(filteredItems, vm.orderBy);

		vm.selectItem(orderedItems[0]._id); // select first item
	}

	if ($rootScope.started) { // if authenticated, check if still authenticated
		$rootScope.authCheck();
	}

	// * * *

	vm.auth = function () { // authenticate (on button click)
		$rootScope.auth();
	};

	vm.shareItem = function (itemID) {
		if ($rootScope.started && !$rootScope.error) {
			vm.selectItem(itemID);

			ShareService.post({ itemID: vm.itemID }, function (response) {
				$window.location.href = '/i/#/' + response.publicID;
			});
		} else {
			bootbox.alert({
				message: 'A Jegyzet vagy Feladat megosztása csak szinkronizáció után lehetséges!',
				size: 'small'
			});
		}
	};

	vm.addItem = function (type) {
		const item = {
			_id: $rootScope.objectId(),
			userID: $rootScope.userID,
			title: null,
			content: null,
			plainContent: null,
			backgroundColor: null,
			state: 2,
			version: 0,
			lastSyncedVersion: null,
			type: type,
			lastMod: Date.now()
		};

		$rootScope.items.push(item);
		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));

		vm.setBgColor(item._id);

		vm.itemID = item._id;
		vm.title = null;
		vm.content = null;

		angular.element('.navbar-collapse').collapse('hide'); // close navbar on mobile
	};

	vm.setBgColor = function (itemID = vm.itemID) { // allows standalone and 'addItem' calls
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === itemID) {
				items[index].backgroundColor = vm.colors[Math.floor(Math.random() * vm.colors.length)];

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.setState = function (state) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				if (localItem.state !== state) { // set new state if current is not the same
					items[index].state = state;
				} else { // set initial state
					items[index].state = 2;
				}

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.saveTitle = function () {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				items[index].title = vm.title;

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.saveContent = function (content, plainContent) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				// user content and plainContent parameters instead of vm.content and vm.plainContent
				items[index].content = content;
				items[index].plainContent = plainContent;

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};
});
