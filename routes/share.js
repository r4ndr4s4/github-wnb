'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const express = require('express');
const router = express.Router();

const share = require('../api/share');

router.post('/share', (req, res, next) => {
	const itemID = req.body.itemID;

	share.getUniquePublicID().then(publicID => { // send back a new publicID
		return share.setPublicID(itemID, publicID); // set the new publicID and return it or get the existing one
	})
	.then(publicID => {
		res.json({ publicID: publicID });
	})
	.catch(err => {
		debug(err);
		res.sendStatus(500);
	});
});

// * * *

router.post('/share/i', (req, res, next) => {
	const publicID = req.body.publicID;

	share.getItem(publicID).then(item => { // get the item with specified publicID
		res.json(item);
	})
	.catch(err => {
		debug(err);
		res.sendStatus(500);
	});
});

module.exports = router;
