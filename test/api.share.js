'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

require('../api/connection');
require('../api/schemas');

const Users = mongoose.model('Users');
const Items = mongoose.model('Items');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const share = require('../api/share');

describe('share api', function () {
	const globalUserID = '58c13c56fd1a8e342d07b7f8';
	const globalItemID = '58c08189e1b8f667c4ff05ee';

	const newUser = { _id: globalUserID };
	const newItem = {
		_id: globalItemID,
		userID: globalUserID,
		title: 'Test Title',
		content: 'Test Content',
		plainContent: 'Test Plain Content',
		backgroundColor: '#FFFFFF',
		state: 1,
		version: 0,
		type: 1,
		lastMod: Date.now()
	};

	let globalPublicID;

	// * * *

	before(function () {
		let promises = [];

		Users.remove({}, (err, res) => {
			if (err) throw new Error('Error while removing Users!');

			promises.push(Users.create(newUser));
		});

		Items.remove({}, (err, res) => {
			if (err) throw new Error('Error while removing Items!');

			promises.push(Items.create(newItem));
		});

		return Promise.all(promises);
	});

	// * * *

	describe('getUniquePublicID', function () {
		it('test getUniquePublicID response type and length', function () {
			return expect(share.getUniquePublicID()).to.eventually.be.a('string').and.have.length(5);
		});
	});

	// * * *

	describe('setPublicID', function () {
		it('test setPublicID with missing itemID parameter', function () {
			return expect(share.setPublicID(null, '1234a')).to.be.rejectedWith('setPublicID itemID or publicID missing');
		});

		it('test setPublicID with missing publicID parameter', function () {
			return expect(share.setPublicID('12a12b12ab1a2b121a21b2a1', null)).to.be.rejectedWith('setPublicID itemID or publicID missing');
		});

		it('test setPublicID to set new publicID', function () {
			return share.getUniquePublicID().then(publicID => {
				globalPublicID = publicID;

				return expect(share.setPublicID(globalItemID, globalPublicID)).to.be.eventually.equal(globalPublicID);
			});
		});

		it('test setPublicID to get existing publicID', function () {
			return expect(share.setPublicID(globalItemID, '1234a')).to.be.eventually.equal(globalPublicID);
		});
	});

	// * * *

	describe('getItem', function () {
		it('test getItem with missing publicID parameter', function () {
			return expect(share.getItem()).to.be.rejectedWith('getItem publicID missing');
		});

		it('test getItem to get existing item', function () {
			return expect(share.getItem(globalPublicID)).to.have.eventually.property('publicID', globalPublicID);
		});
	});
});
