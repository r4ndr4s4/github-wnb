'use strict';

angular.module('wnb').run(function ($rootScope, $window, $location, $interval, AuthService, SyncService) {
	$rootScope.objectId = (m = Math, d = Date, h = 16, s = s => m.floor(s).toString(h)) => s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));
	$rootScope.localStorage = $window.localStorage; // avoid getting no-undef in ESLint

	if (!$rootScope.localStorage.getItem('wnb.userID')) {
		$rootScope.userID = $rootScope.objectId();

		$rootScope.localStorage.setItem('wnb.userID', angular.toJson($rootScope.userID));
	} else {
		$rootScope.userID = angular.fromJson($rootScope.localStorage.getItem('wnb.userID'));
	}

	if (!$rootScope.localStorage.getItem('wnb.items')) {
		$rootScope.items = [];

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	} else {
		$rootScope.items = angular.fromJson($rootScope.localStorage.getItem('wnb.items'));
	}

	$rootScope.started = $rootScope.localStorage.getItem('wnb.auth'); // used in HTML and controller also

	// * * *

	$rootScope.auth = function () { // called after login with Google OAuth
		AuthService.post({ clientID: $rootScope.userID }, function (response) {
			if (response.error === 'not-authenticated') { // if not authenticated, redirect to /login which starts the authentication process with Google OAuth
				$window.location.href = '/login';
			} else { // if authenticated, update user's current state with response and call sync
				$rootScope.localStorage.setItem('wnb.auth', true);
				$rootScope.started = true;

				$rootScope.syncClients(response.user.clientIDs);

				$window.location.href = '/#/';
			}
		});
	};

	$rootScope.authCheck = function () { // called after login with Google OAuth and in every sync to get the user's current clientIDs
		AuthService.post({ clientID: $rootScope.userID }, function (response) {
			if (response.error === 'not-authenticated') { // if not authenticated, stop syncing
				$rootScope.error = true;
			} else { // if authenticated, update user's current state with response and call sync
				$rootScope.syncClients(response.user.clientIDs);
			}
		});
	};

	$rootScope.syncClients = function (clientIDs) {
		$rootScope.localStorage.setItem('wnb.clientIDs', clientIDs);
		$rootScope.error = false;

		clientIDs.forEach(clientID => {
			$rootScope.sync(clientID); // standalone sync calls for different clientIDs
		});

		if (!$rootScope.interval) { // if timer is not running
			$rootScope.interval = $interval(function () { // run sync every half minute
				if (!$rootScope.error) { // if user is still authenticated
					$rootScope.authCheck();
				}
			}, 30000);
		}
	};

	// * * *

	$rootScope.sync = function (userID) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (!localItem.content && localItem.version !== localItem.lastSyncedVersion && localItem.lastSyncedVersion !== null) { // hide empty, updated items
				items[index].content = '-';
				items[index].plainContent = '-';
				items[index].state = 4;
			}
		});

		const updated = $rootScope.items.filter(item => item.userID === userID && item.version !== item.lastSyncedVersion && item.lastSyncedVersion !== null);
		const new_ = $rootScope.items.filter(item => item.userID === userID && item.lastSyncedVersion === null && item.content); // don't sync new, empty items

		const versions = $rootScope.items.filter(item => item.userID === userID && item.version === item.lastSyncedVersion && item.state < 4); // don't sync hidden items
		let versions_ = [];

		versions.forEach(item => {
			versions_.push({
				_id: item._id,
				version: item.version
			});
		});

		const request = { userID: userID, items: { updated: updated, new: new_ }, versions: versions_ };

		SyncService.post(request, function (response) {
			response.items.updated.forEach(responseItem => { // updated items
				responseItem.lastSyncedVersion = responseItem.version;

				$rootScope.items.forEach((localItem, index, items) => {
					if (responseItem._id === localItem._id) {
						items[index] = responseItem;
					}
				});

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});

			response.items.new.forEach(responseItem => { // added items
				responseItem.lastSyncedVersion = responseItem.version;

				$rootScope.items.push(responseItem);

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});

			response.versions.forEach(responseVersion => { // versions
				$rootScope.items.forEach((localItem, index, items) => {
					if (responseVersion._id === localItem._id) {
						items[index].lastSyncedVersion = responseVersion.version;
					}
				});

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});
		});
	};
});
