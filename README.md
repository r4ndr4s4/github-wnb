### Jegyzet és feladatlista alkalmazás bejelentkezett eszközök közötti szinkronizációval

Jegyzet és feladatlista alkalmazás, mely autentikáció nélkül localStorage-be menti a módosításokat, Google OAuth-al történő autentikáció után pedig a szerver REST API-jával kommunikálva valósítja meg azok mentését, több bejelentkezett eszköz esetén azok közötti szinkronizációját.

_Használva: Express, Passport, Mongoose, AngularJS, Quill, Bootstrap, Font Awesome, ESLint, Gulp, Mocha, Chai, Karma_
