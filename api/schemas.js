'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Users = new Schema({
	name: {
		type: String,
		required: true
	},
	googleID: {
		type: String,
		required: true,
		unique: true
	},
	clientIDs: {
		type: [Schema.Types.ObjectId] // not required, because clientIDs inserted later from client side
	}
});

const Items = new Schema({
	userID: {
		type: Schema.Types.ObjectId,
		ref: 'Users',
		required: true
	},
	title: {
		type: String
	},
	content: {
		type: String,
		required: true
	},
	plainContent: {
		type: String,
		required: true
	},
	backgroundColor: {
		type: String,
		required: true
	},
	state: {
		type: Number,
		required: true
	},
	publicID: {
		type: String
	},
	version: {
		type: Number,
		required: true
	},
	type: {
		type: Number,
		required: true
	},
	lastMod: {
		type: Date,
		required: true
	}
});

module.exports = mongoose.model('Users', Users, 'Users'); // 2nd 'Users' added to avoid infer from model name as 'users' instead 'Users'
module.exports = mongoose.model('Items', Items, 'Items'); // 2nd 'Items' added to avoid infer from model name as 'items' instead 'Items'
