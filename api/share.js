'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

require('./connection');
require('./schemas');

const Items = mongoose.model('Items');

const getUniquePublicID = function () {
	const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
	const publicIDlength = 5; // 4 numbers + 1 character

	const randomNumber = Math.floor((Math.random() * Math.pow(10, (publicIDlength - 1)))); // generate number
	const randomLetter = alphabet[Math.floor(Math.random() * alphabet.length)]; // select letter
	const randomLetterPlace = Math.floor(Math.random() * publicIDlength); // generate position of letter

	let randomString;

	randomString = randomNumber.toString();

	// add a zero if it is shorter then the specified length
	for (let i = randomString.length; i < (publicIDlength - 1); i++) {
		randomString += '0';
	}

	randomString = randomString.slice(0, randomLetterPlace) + randomLetter + randomString.slice(randomLetterPlace); // insert to generated position

	return new Promise((resolve, reject) => {
		Items.count({ publicID: randomString }, (err, res) => {
			if (err) reject(new Error('Promise Error: ' + err));

			if (res === 0) { // check if unique
				resolve(randomString);
			} else {
				resolve(getUniquePublicID()); // generate other one
			}
		});
	});
};

// * * *

const setPublicID = function (itemID, publicID) {
	if (!itemID || !publicID) return new Promise((resolve, reject) => reject(new Error('setPublicID itemID or publicID missing')));

	return new Promise((resolve, reject) => {
		Items.findById(itemID, (err, res) => {
			if (err) reject(new Error('Promise Error: ' + err));

			if (!res.publicID) { // set the new publicID and return it
				res.publicID = publicID;

				res.save((err) => {
					if (err) reject(new Error('Promise Error: ' + err));

					resolve(publicID);
				});
			} else { // get the existing publicID
				resolve(res.publicID);
			}
		});
	});
};

// * * *

const getItem = function (publicID) {
	if (!publicID) return new Promise((resolve, reject) => reject(new Error('getItem publicID missing')));

	return new Promise((resolve, reject) => {
		Items.findOne({ publicID: publicID }, (err, res) => {
			if (err) reject(new Error('Promise Error: ' + err));

			resolve(res);
		});
	});
};

module.exports = { getUniquePublicID, setPublicID, getItem };
