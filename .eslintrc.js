module.exports = {
    "extends": [
        "standard",
        "angular"
    ],
    "plugins": [
        "standard",
        "promise"
    ],
    "rules": {
    	"semi": ["error", "always"],
    	"no-tabs": "off",
    	"indent": ["error", "tab"],
        "no-unused-vars": "warn"
    },
    "env": {
        "mocha": true,
        "jquery": true
    },
    "globals": {
        "bootbox": true
    }
};
