'use strict';

angular.module('wnb').config(function ($locationProvider) {
	$locationProvider.hashPrefix('');
});

angular.module('wnb').config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/html/editor/all.html'
		})
		.when('/starred', {
			templateUrl: '/html/editor/starred.html'
		})
		.when('/deleted', {
			templateUrl: '/html/editor/deleted.html'
		})
		.when('/auth', {
			templateUrl: '/html/editor/all.html',
			controller: 'AuthController as AuthCtrl'
		});
});

angular.module('wnb').config(function (ngQuillConfigProvider) {
	ngQuillConfigProvider.set({
		placeholder: ' ', // empty (and disabled) as default
		modules: {
			toolbar: [
				['bold', 'italic', 'underline', 'strike', { 'background': [] }],
				[ { 'header': 1 }, { 'header': 2 }, { 'list': 'bullet' }, 'task-list' ],
				['blockquote', 'code-block', 'link'],
				[ { 'indent': '-1' }, { 'indent': '+1' } ],
				['clean']
			],
			syntax: true, // use highlight.js
			'task-list': true
		}
	});
});
