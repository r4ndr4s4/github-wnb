'use strict';

const debug = require('debug')('wnb:server');

const environment = process.env.SETTINGS;

let settings;

if (environment === 'development') {
	settings = require('./development');
} else if (environment === 'production') {
	settings = require('./production');
}

module.exports = settings;
