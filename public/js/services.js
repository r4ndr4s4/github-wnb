'use strict';

angular.module('wnb').factory('AuthService', function ($resource) {
	return $resource('/auth', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('SyncService', function ($resource) {
	return $resource('/sync', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('ShareService', function ($resource) {
	return $resource('/share', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('ViewerService', function ($resource) {
	return $resource('/share/i', {}, {
		post: {
			method: 'POST'
		}
	});
});
