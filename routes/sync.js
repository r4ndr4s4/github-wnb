'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const express = require('express');
const router = express.Router();

const sync = require('../api/sync');

router.post('/sync', (req, res, next) => {
	const userID = req.body.userID;
	const updatedItems = req.body.items.updated;
	const newItems = req.body.items.new;
	const versions = req.body.versions;

	let updatedVersions = []; // versions from updated items for response
	let newVersions = []; // versions from added items for response

	updatedItems.forEach(item => {
		updatedVersions.push({
			_id: item._id,
			version: item.version
		});
	});

	newItems.forEach(item => {
		newVersions.push({
			_id: item._id,
			version: item.version
		});
	});

	const itemsVersions = updatedVersions.concat(newVersions); // for findNewItems and response

	sync.updateOldItems(updatedItems).then(() => {
		return sync.addNewItems(newItems);
	})
	.then(() => {
		return sync.findUpdatedItems(versions);
	})
	.then(updatedItems_ => {
		const updated = updatedItems_.filter(item => item); // remove null values

		if (updatedItems.length > 0 || newItems.length > 0 || versions.length > 0) { // if there are items or versions specified
			sync.findNewItems(userID, itemsVersions.concat(versions)).then(newItems => { // findNewItems needs all versions in client to check for new items on server
				const newItems_ = newItems.filter(item => item); // remove null values

				res.json({ items: { updated: updated, new: newItems_ }, versions: itemsVersions });
			})
			.catch(err => {
				debug(err);
				res.sendStatus(500);
			});
		} else { // else get all items
			sync.getAllItems(userID).then(allItems => {
				res.json({ items: { updated: updated, new: allItems }, versions: itemsVersions });
			})
			.catch(err => {
				debug(err);
				res.sendStatus(500);
			});
		}
	})
	.catch(err => {
		debug(err);
		res.sendStatus(500);
	});
});

module.exports = router;
