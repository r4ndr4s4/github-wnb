'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

require('./connection');
require('./schemas');

const Users = mongoose.model('Users');

passport.use(new GoogleStrategy({
	clientID: settings.googleClientID,
	clientSecret: settings.googleClientSecret,
	callbackURL: settings.googleCallbackURL
}, function (accessToken, refreshToken, profile, done) {
	Users.findOne({ googleID: profile.id }, (err, res) => {
		if (err) return done(err, null);

		if (res) { // if user already exist
			return done(null, res); // return user's state
		} else { // create user if haven't logged in yet
			const user = {
				name: profile.name.givenName,
				googleID: profile.id,
				clientIDs: []
			};

			Users.create(user, (err, res) => {
				return done(err, user); // return user's state
			});
		}
	});
}));

passport.serializeUser(function (user, done) {
	done(null, user.googleID); // serialize unique googleID
});

passport.deserializeUser(function (obj, done) {
	Users.findOne({ googleID: obj }, (err, res) => { // find user's current state by serialized googleID on every /auth call
		done(err, res);
	});
});

// * * *

const addNewClientID = function (googleID, clientID) {
	return new Promise((resolve, reject) => {
		Users.findOne({ googleID: googleID }, (err, res) => {
			if (err) reject(new Error('Promise Error: ' + err));

			if (res.clientIDs.indexOf(clientID) === -1) { // add new clientID
				res.clientIDs.push(clientID);

				res.save((err, res) => {
					if (err) reject(new Error('Promise Error: ' + err));

					resolve(res); // return user's current state
				});
			} else {
				resolve(res); // return user's current state
			}
		});
	});
};

module.exports = { addNewClientID };
