'use strict';

const debug = require('debug')('wnb:server');
const settings = require('./settings');

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const session = require('express-session');

const passport = require('passport');
require('./api/auth');

const index = require('./routes/index');
const item = require('./routes/item');
const share = require('./routes/share');
const sync = require('./routes/sync');
const auth = require('./routes/auth');

const app = express();

// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ secret: 'wnb', resave: true, saveUninitialized: true }));

app.use(passport.initialize());
app.use(passport.session());

app.use(index);
app.use(item);
app.use(share);
app.use(sync);
app.use(auth);

app.use((req, res, next) => {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use((err, req, res, next) => res.sendStatus(err.status || 500));

module.exports = app;
