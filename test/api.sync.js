'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

require('../api/connection');
require('../api/schemas');

const Users = mongoose.model('Users');
const Items = mongoose.model('Items');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const clone = require('lodash.clone');

const sync = require('../api/sync');

describe('sync api', function () {
	const globalUserID = '58c13c56fd1a8e342d07b7f8';
	const globalItemID1 = '58c08189e1b8f667c4ff05ee';
	const globalItemID2 = '58c08189e1b8f667c4ff05ef';
	const globalItemID3 = '58c08189e1b8f667c4ff05fa';

	const newUser = { _id: globalUserID };
	const newItem1 = {
		_id: globalItemID1,
		userID: globalUserID,
		title: 'Test Title 1',
		content: 'Test Content 1',
		plainContent: 'Test Plain Content 1',
		backgroundColor: '#FFFFFF',
		state: 1,
		publicID: '1234a',
		version: 0,
		type: 1,
		lastMod: Date.now()
	};
	const newItem2 = {
		_id: globalItemID2,
		userID: globalUserID,
		title: 'Test Title 2',
		content: 'Test Content 2',
		plainContent: 'Test Plain Content 2',
		backgroundColor: '#FFFFFF',
		state: 1,
		publicID: '1234b',
		version: 0,
		type: 1,
		lastMod: Date.now()
	};
	const newItem3 = {
		_id: globalItemID3,
		userID: globalUserID,
		title: 'Test Title 3',
		content: 'Test Content 3',
		plainContent: 'Test Plain Content 3',
		backgroundColor: '#FFFFFF',
		state: 1,
		publicID: '1234c',
		version: 0,
		type: 1,
		lastMod: Date.now()
	};

	// * * *

	before(function () {
		let promises = [];

		Users.remove({}, (err, res) => {
			if (err) throw new Error('Error while removing Users!');

			promises.push(Users.create(newUser));
		});

		Items.remove({}, (err, res) => {
			if (err) throw new Error('Error while removing Items!');

			promises.push(Items.create(newItem1, newItem2));
		});

		return Promise.all(promises);
	});

	// * * *

	describe('updateOldItems', function () {
		it('test updateOldItems with empty items parameter', function () {
			return expect(sync.updateOldItems([])).to.eventually.have.length(0); // TODO: with missing items parameter
		});

		it('test updateOldItems with NOK item (NOT less version, equal lastSyncedVersion)', function () {
			const newItem1Request = requestHelper(newItem1);

			return sync.updateOldItems([newItem1Request]).then(() => {
				return expect(Items.findOne({ _id: newItem1Request._id, title: newItem1Request.title }).count()).to.be.eventually.equal(0);
			});
		});

		it('test updateOldItems with NOK item (less version, NOT equal lastSyncedVersion)', function () {
			const newItem1Request = requestHelper(newItem1, 1, 1);

			return sync.updateOldItems([newItem1Request]).then(() => {
				return expect(Items.findOne({ _id: newItem1Request._id, title: newItem1Request.title }).count()).to.be.eventually.equal(0);
			});
		});

		it('test updateOldItems with OK item', function () {
			const newItem1Request = requestHelper(newItem1, 1);

			return sync.updateOldItems([newItem1Request]).then(() => {
				return expect(Items.findOne({ _id: newItem1Request._id, title: newItem1Request.title }).count()).to.be.eventually.equal(1);
			});
		});

		// TODO: with many OK items
		// TODO: with OK and NOK items also
	});

	// * * *

	describe('addNewItems', function () {
		it('test addNewItems with empty items parameter', function () {
			return expect(sync.addNewItems([])).to.eventually.have.length(0); // TODO: with missing items parameter
		});

		it('test addNewItems with item', function () {
			return sync.addNewItems([newItem3]).then(() => {
				return expect(Items.findById(globalItemID3).count()).to.be.eventually.equal(1);
			});
		});

		// TODO: with many items
	});

	// * * *

	describe('findUpdatedItems', function () {
		it('test findUpdatedItems with empty versions parameter', function () {
			return expect(sync.findUpdatedItems([])).to.eventually.have.length(0); // TODO: with missing versions parameter
		});

		it('test findUpdatedItems with NOK item (NOT greater version)', function () {
			const versions = [{
				_id: globalItemID2,
				version: 0
			}];

			return expect(sync.findUpdatedItems(versions)).to.be.eventually.deep.equal([null]); // TODO: error message if assert fail
		});

		it('test findUpdatedItems with OK item', function () { // TODO: chaiAsPromised style
			const versions = [{
				_id: globalItemID1,
				version: 0
			}];

			return sync.findUpdatedItems(versions).then(items => {
				const itemID = items[0]._id.toString();
				const version = items[0].version;

				expect(itemID).to.equal(globalItemID1);
				expect(version).to.equal(1);
			});
		});

		// TODO: with many OK items
		// TODO: with OK and NOK items also
	});

	// * * *

	describe('findNewItems', function () {
		it('test findNewItems with missing userID parameter', function () {
			const versions = [{
				_id: '12a12b12ab1a2b121a21b2a1',
				version: 0
			}];

			return expect(sync.findNewItems(null, versions)).to.be.rejectedWith('findNewItems userID missing or versions empty');
		});

		it('test findNewItems with empty versions parameter', function () {
			return expect(sync.findNewItems('12a12b12ab1a2b121a21b2a1', [])).to.be.rejectedWith('findNewItems userID missing or versions empty'); // TODO: with missing versions parameter
		});

		it('test findNewItems with OK response item', function () { // TODO: chaiAsPromised style
			const versions = [{
				_id: globalItemID1,
				version: 1
			}, {
				_id: globalItemID2,
				version: 0
			}];

			return sync.findNewItems(globalUserID, versions).then(items => {
				if (items.length > 1) throw new Error('More response items: expected ' + items.length + ' to equal 1');

				const itemID = items[0]._id.toString();

				expect(itemID).to.equal(globalItemID3);
			});
		});

		// TODO: with many OK response items
	});

	// * * *

	describe('getAllItems', function () {
		it('test getAllItems with missing userID parameter', function () {
			return expect(sync.getAllItems()).to.be.rejectedWith('getAllItems userID missing');
		});

		it('test getAllItems with OK response items', function () { // TODO: chaiAsPromised style
			return sync.getAllItems(globalUserID).then(items => {
				if (items.length !== 3) throw new Error('More/less response items: expected ' + items.length + ' to equal 3');

				const itemIDs = [globalItemID1, globalItemID2, globalItemID3];

				items.forEach(item => {
					// if (typeof itemIDs.find(itemID => itemID === item._id.toString()) === 'undefined') {
					if (itemIDs.indexOf(item._id.toString()) === -1) {
						throw new Error('Not expected response item!');
					}
				});
			});
		});
	});
});

function requestHelper (object, version = 0, lastSyncedVersion = 0) {
	let requestObject = clone(object);

	requestObject.title += '_';
	requestObject.version = version;
	requestObject.lastSyncedVersion = lastSyncedVersion;

	return requestObject;
}
