'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const express = require('express');
const router = express.Router();

const passport = require('passport');

const auth = require('../api/auth');

router.get('/login', passport.authenticate('google', {
	scope: ['profile']
}));

router.get('/login/return', passport.authenticate('google', {
	successRedirect: '/#/auth', // calls client side authentication and save clientID
	failureRedirect: '/login'
}));

router.post('/auth', (req, res, next) => { // called after login with Google OAuth and in every sync to get the user's current clientIDs
	if (req.isAuthenticated()) { return next(); }

	res.json({ error: 'not-authenticated' });
}, (req, res, next) => {
	const clientID = req.body.clientID;

	auth.addNewClientID(req.user.googleID, clientID).then(() => {
		res.json({ user: req.user });
	})
	.catch(err => {
		debug(err);
		res.sendStatus(500);
	});
});

module.exports = router;
