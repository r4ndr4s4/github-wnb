'use strict';

const gulp = require('gulp');
const pump = require('pump');

const concat = require('gulp-concat');
const order = require('gulp-order');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const eslint = require('gulp-eslint');
const babel = require('gulp-babel');
const watch = require('gulp-watch');

// gulp task and watch for client files
gulp.task('default-client', function (err) {
	pump([
		gulp.src(['./public/js/*.js', '!./public/js/all.js', '!./public/js/all.min.js', '!./public/js/material.min.js', '!./public/js/quill-task-list.js']),
		eslint(),
		eslint.format(),
		order([
			'index.js',
			'services.js',
			'config.js',
			'run.js',
			'editor.controller.js',
			'viewer.controller.js',
			'auth.controller.js'
		]),
		concat('all.js'),
		gulp.dest('./public/js/'),
		babel({
			presets: ['es2015']
		}),
		uglify(),
		rename({
			suffix: '.min'
		}),
		gulp.dest('./public/js/')
	], err);
});

gulp.task('watch-client', function () {
	watch(['./public/js/*.js', './gulpfile.js', '!./public/js/all.js', '!./public/js/all.min.js', '!./public/js/material.min.js', '!./public/js/quill-task-list.js'], function () {
		gulp.start('default-client');
	});
});

// * * *

// gulp task and watch for client tests
gulp.task('default-client-tests', function (err) {
	pump([
		gulp.src(['./public/js/test/*.js']),
		eslint(),
		eslint.format()
	], err);
});

gulp.task('watch-client-tests', function () {
	watch(['./public/js/test/*.js', './gulpfile.js'], function () {
		gulp.start('default-client-tests');
	});
});

// * * *

// gulp task and watch for server files
gulp.task('default-server', function (err) {
	pump([
		gulp.src(['./index.js', './gulpfile.js', './api/*.js', './bin/www', './routes/*.js', './test/*.js']),
		eslint(),
		eslint.format()
	], err);
});

gulp.task('watch-server', function () {
	watch(['./index.js', './settings.json', './gulpfile.js', './api/*.js', './bin/www', './routes/*.js', './test/*.js'], function () {
		gulp.start('default-server');
	});
});
