'use strict';

angular.module('wnb', ['ngRoute', 'ngResource', 'ngSanitize', 'ngQuill']);

'use strict';

angular.module('wnb').factory('AuthService', function ($resource) {
	return $resource('/auth', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('SyncService', function ($resource) {
	return $resource('/sync', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('ShareService', function ($resource) {
	return $resource('/share', {}, {
		post: {
			method: 'POST'
		}
	});
});

angular.module('wnb').factory('ViewerService', function ($resource) {
	return $resource('/share/i', {}, {
		post: {
			method: 'POST'
		}
	});
});

'use strict';

angular.module('wnb').config(function ($locationProvider) {
	$locationProvider.hashPrefix('');
});

angular.module('wnb').config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/html/editor/all.html'
		})
		.when('/starred', {
			templateUrl: '/html/editor/starred.html'
		})
		.when('/deleted', {
			templateUrl: '/html/editor/deleted.html'
		})
		.when('/auth', {
			templateUrl: '/html/editor/all.html',
			controller: 'AuthController as AuthCtrl'
		});
});

angular.module('wnb').config(function (ngQuillConfigProvider) {
	ngQuillConfigProvider.set({
		placeholder: ' ', // empty (and disabled) as default
		modules: {
			toolbar: [
				['bold', 'italic', 'underline', 'strike', { 'background': [] }],
				[ { 'header': 1 }, { 'header': 2 }, { 'list': 'bullet' }, 'task-list' ],
				['blockquote', 'code-block', 'link'],
				[ { 'indent': '-1' }, { 'indent': '+1' } ],
				['clean']
			],
			syntax: true, // use highlight.js
			'task-list': true
		}
	});
});

'use strict';

angular.module('wnb').run(function ($rootScope, $window, $location, $interval, AuthService, SyncService) {
	$rootScope.objectId = (m = Math, d = Date, h = 16, s = s => m.floor(s).toString(h)) => s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));
	$rootScope.localStorage = $window.localStorage; // avoid getting no-undef in ESLint

	if (!$rootScope.localStorage.getItem('wnb.userID')) {
		$rootScope.userID = $rootScope.objectId();

		$rootScope.localStorage.setItem('wnb.userID', angular.toJson($rootScope.userID));
	} else {
		$rootScope.userID = angular.fromJson($rootScope.localStorage.getItem('wnb.userID'));
	}

	if (!$rootScope.localStorage.getItem('wnb.items')) {
		$rootScope.items = [];

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	} else {
		$rootScope.items = angular.fromJson($rootScope.localStorage.getItem('wnb.items'));
	}

	$rootScope.started = $rootScope.localStorage.getItem('wnb.auth'); // used in HTML and controller also

	// * * *

	$rootScope.auth = function () { // called after login with Google OAuth
		AuthService.post({ clientID: $rootScope.userID }, function (response) {
			if (response.error === 'not-authenticated') { // if not authenticated, redirect to /login which starts the authentication process with Google OAuth
				$window.location.href = '/login';
			} else { // if authenticated, update user's current state with response and call sync
				$rootScope.localStorage.setItem('wnb.auth', true);
				$rootScope.started = true;

				$rootScope.syncClients(response.user.clientIDs);

				$window.location.href = '/#/';
			}
		});
	};

	$rootScope.authCheck = function () { // called after login with Google OAuth and in every sync to get the user's current clientIDs
		AuthService.post({ clientID: $rootScope.userID }, function (response) {
			if (response.error === 'not-authenticated') { // if not authenticated, stop syncing
				$rootScope.error = true;
			} else { // if authenticated, update user's current state with response and call sync
				$rootScope.syncClients(response.user.clientIDs);
			}
		});
	};

	$rootScope.syncClients = function (clientIDs) {
		$rootScope.localStorage.setItem('wnb.clientIDs', clientIDs);
		$rootScope.error = false;

		clientIDs.forEach(clientID => {
			$rootScope.sync(clientID); // standalone sync calls for different clientIDs
		});

		if (!$rootScope.interval) { // if timer is not running
			$rootScope.interval = $interval(function () { // run sync every half minute
				if (!$rootScope.error) { // if user is still authenticated
					$rootScope.authCheck();
				}
			}, 30000);
		}
	};

	// * * *

	$rootScope.sync = function (userID) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (!localItem.content && localItem.version !== localItem.lastSyncedVersion && localItem.lastSyncedVersion !== null) { // hide empty, updated items
				items[index].content = '-';
				items[index].plainContent = '-';
				items[index].state = 4;
			}
		});

		const updated = $rootScope.items.filter(item => item.userID === userID && item.version !== item.lastSyncedVersion && item.lastSyncedVersion !== null);
		const new_ = $rootScope.items.filter(item => item.userID === userID && item.lastSyncedVersion === null && item.content); // don't sync new, empty items

		const versions = $rootScope.items.filter(item => item.userID === userID && item.version === item.lastSyncedVersion && item.state < 4); // don't sync hidden items
		let versions_ = [];

		versions.forEach(item => {
			versions_.push({
				_id: item._id,
				version: item.version
			});
		});

		const request = { userID: userID, items: { updated: updated, new: new_ }, versions: versions_ };

		SyncService.post(request, function (response) {
			response.items.updated.forEach(responseItem => { // updated items
				responseItem.lastSyncedVersion = responseItem.version;

				$rootScope.items.forEach((localItem, index, items) => {
					if (responseItem._id === localItem._id) {
						items[index] = responseItem;
					}
				});

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});

			response.items.new.forEach(responseItem => { // added items
				responseItem.lastSyncedVersion = responseItem.version;

				$rootScope.items.push(responseItem);

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});

			response.versions.forEach(responseVersion => { // versions
				$rootScope.items.forEach((localItem, index, items) => {
					if (responseVersion._id === localItem._id) {
						items[index].lastSyncedVersion = responseVersion.version;
					}
				});

				$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
			});
		});
	};
});

'use strict';

angular.module('wnb').controller('EditorController', function ($scope, $rootScope, $window, $filter, orderByFilter, ShareService) {
	let vm = this;

	vm.colors = ['red', 'blue', 'green', 'yellow', 'brown', 'grey', 'teal'];

	vm.orderBy = ['state', '-lastMod']; // used in HTML and controller also
	vm.filter = () => (item) => item.state < 3;  // used in HTML and controller also

	// * * *

	vm.selectItem = function (itemID) {
		const items = angular.fromJson($rootScope.localStorage.getItem('wnb.items'));
		const item = items.find(item => item._id === itemID);

		vm.itemID = itemID; // set as selected item
		vm.type = item.type; // set for note/task type
		vm.state = item.state; // set for task checked state
		vm.title = item.title; // set for edit mode
		vm.content = item.content; // set for edit mode

		angular.element('.navbar-collapse').collapse('hide'); // close navbar on mobile
	};

	$rootScope.items.forEach((localItem, index, items) => {
		if (!localItem.content && localItem.lastSyncedVersion === null) {
			items[index].state = 4; // hide empty, new items
		}
	});

	const filteredItems = $filter('filter')($rootScope.items, vm.filter());

	if (filteredItems.length > 0) {
		const orderedItems = orderByFilter(filteredItems, vm.orderBy);

		vm.selectItem(orderedItems[0]._id); // select first item
	}

	if ($rootScope.started) { // if authenticated, check if still authenticated
		$rootScope.authCheck();
	}

	// * * *

	vm.auth = function () { // authenticate (on button click)
		$rootScope.auth();
	};

	vm.shareItem = function (itemID) {
		if ($rootScope.started && !$rootScope.error) {
			vm.selectItem(itemID);

			ShareService.post({ itemID: vm.itemID }, function (response) {
				$window.location.href = '/i/#/' + response.publicID;
			});
		} else {
			bootbox.alert({
				message: 'A Jegyzet vagy Feladat megosztása csak szinkronizáció után lehetséges!',
				size: 'small'
			});
		}
	};

	vm.addItem = function (type) {
		const item = {
			_id: $rootScope.objectId(),
			userID: $rootScope.userID,
			title: null,
			content: null,
			plainContent: null,
			backgroundColor: null,
			state: 2,
			version: 0,
			lastSyncedVersion: null,
			type: type,
			lastMod: Date.now()
		};

		$rootScope.items.push(item);
		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));

		vm.setBgColor(item._id);

		vm.itemID = item._id;
		vm.title = null;
		vm.content = null;

		angular.element('.navbar-collapse').collapse('hide'); // close navbar on mobile
	};

	vm.setBgColor = function (itemID = vm.itemID) { // allows standalone and 'addItem' calls
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === itemID) {
				items[index].backgroundColor = vm.colors[Math.floor(Math.random() * vm.colors.length)];

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.setState = function (state) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				if (localItem.state !== state) { // set new state if current is not the same
					items[index].state = state;
				} else { // set initial state
					items[index].state = 2;
				}

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.saveTitle = function () {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				items[index].title = vm.title;

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};

	vm.saveContent = function (content, plainContent) {
		$rootScope.items.forEach((localItem, index, items) => {
			if (localItem._id === vm.itemID) {
				// user content and plainContent parameters instead of vm.content and vm.plainContent
				items[index].content = content;
				items[index].plainContent = plainContent;

				items[index].version++;
				items[index].lastMod = Date.now();
			}
		});

		$rootScope.localStorage.setItem('wnb.items', angular.toJson($rootScope.items));
	};
});

'use strict';

angular.module('wnb').controller('ViewerController', function ($scope, $sce, $location, ViewerService) {
	const url = $location.url();
	const publicID = url.slice(1, url.length);

	let vm = this;

	ViewerService.post({ publicID: publicID }, function (response) {
		vm.content = $sce.trustAsHtml(response.content);
		vm.title = response.title;
	});
});

'use strict';

angular.module('wnb').controller('AuthController', function ($rootScope) {
	$rootScope.auth();
});
