'use strict';

const debug = require('debug')('wnb:server');
const settings = require('../settings');

const mongoose = require('mongoose');

require('./connection');
require('./schemas');

const Items = mongoose.model('Items');

const updateOldItems = function (items) {
	if (items.length === 0) return new Promise((resolve, reject) => resolve([])); // if parameter empty, return with an empty array

	let promises = [];

	items.forEach(item => {
		promises.push(new Promise((resolve, reject) => {
			// update items with item received from client where ID is specified, version is less than specified and equal to lastSyncedVersion
			Items.findOneAndUpdate({ _id: item._id, $and: [ { version: { $lt: item.version } }, { version: { $eq: item.lastSyncedVersion } } ] }, item, { runValidators: true }, (err, res) => {
				if (err) reject(new Error('Promise Error: ' + err));

				resolve();
			});
		}));
	});

	return Promise.all(promises);
};

// * * *

const addNewItems = function (items) {
	if (items.length === 0) return new Promise((resolve, reject) => resolve([])); // if parameter empty, return with an empty array

	return new Promise((resolve, reject) => {
		Items.create(items, (err, res) => { // insert all items
			if (err) reject(new Error('Promise Error: ' + err));

			resolve();
		});
	});
};

// * * *

const findUpdatedItems = function (versions) {
	if (versions.length === 0) return new Promise((resolve, reject) => resolve([])); // if parameter empty, return with an empty array

	let promises = [];

	versions.forEach(version => {
		promises.push(new Promise((resolve, reject) => {
			Items.findOne({ _id: version._id, version: { $gt: version.version } }, (err, res) => { // find items with specified ID and greater version than received from client
				if (err) reject(new Error('Promise Error: ' + err));

				resolve(res);
			});
		}));
	});

	return Promise.all(promises);
};

// * * *

const findNewItems = function (userID, versions) {
	if (!userID || versions.length === 0) return new Promise((resolve, reject) => reject(new Error('findNewItems userID missing or versions empty')));

	return new Promise((resolve, reject) => {
		Items.find({ _id: { $nin: versions }, userID: userID }, (err, res) => { // find items with specified userID and ID not in received IDs from client
			if (err) reject(new Error('Promise Error: ' + err));

			resolve(res);
		});
	});
};

// * * *

const getAllItems = function (userID) {
	if (!userID) return new Promise((resolve, reject) => reject(new Error('getAllItems userID missing')));

	return new Promise((resolve, reject) => {
		Items.find({ userID: userID }, (err, res) => { // find items with specified userID
			if (err) reject(new Error('Promise Error: ' + err));

			resolve(res);
		});
	});
};

module.exports = { updateOldItems, addNewItems, findUpdatedItems, findNewItems, getAllItems };
